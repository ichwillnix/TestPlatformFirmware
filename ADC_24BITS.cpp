#include <Arduino.h>
#include "ADC_24BITS.h"
#include "Error.h"

extern byte is1580;

ADC_24BITS::ADC_24BITS(){
	_adc0_channel_lock = 0;
}

NAU7802 adc0_24bits(0);
NAU7802 adc1_24bits(1);

void ADC_24BITS::init(){
  if(is1580){
    if (!adc0_24bits.init() || !adc1_24bits.init()) while (1){ red_led(1); };
  }else{
    if (!adc0_24bits.init()) while (1){ red_led(1); };
  }
}

//Call this function until it returns true (avoid busy waits)
byte ADC_24BITS::get_thrust(float *value){
	return adc0_24bits.get_adc_val(value, 0);
}

//Call this function until it returns true (avoid busy waits)
byte ADC_24BITS::get_left(float *value){
	return adc0_24bits.get_adc_val(value, 1);
}

//Call this function until it returns true (avoid busy waits)
byte ADC_24BITS::get_right(float *value){
	return adc1_24bits.get_adc_val(value, 0);
}

//Returns the winding resistance value
//Call this function until it returns true (avoid busy waits)
byte ADC_24BITS::get_ohms(float *value, byte *result_state){
	//Take a number of readings for averaging
	float voltage;
	static byte counter;
	static float voltage_sum;
	static byte step = 0;

	switch (step)
	{
	case 0:
		voltage_sum = 0.0;
		counter = 0;
		step++;
		break;
	case 1:
		if (adc1_24bits.get_adc_val(&voltage, 1)){
			voltage_sum += voltage;
			if (++counter == 100){
				voltage = 0.01 * voltage_sum;
				*result_state = 0;

				//Calculate resistance on ohmmeter. Layout is VCC-120-R-120-GND
				//(ie R voltage is measured by the ADC, and two precision (0.1%)
				//120Ohm resistors are placed on either side connected to power.
				voltage = -(240.0*voltage) / (voltage - 5.0);
				if (voltage > 235.0){
					*result_state = 2;
				}else{
					if (voltage < 0.005) *result_state = 1; //To small to measure accurately, because of trace resistance and other factors.
				}

				*value = voltage; //in ohms.
				step=0;
				return true;
			}
		}
		break;
	}
	return false;
}
