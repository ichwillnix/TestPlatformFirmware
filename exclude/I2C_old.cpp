/*
  I2C.cpp - Library for common i2c functions.
  Created by Tyto Robotics Inc., March 16, 2015.
  Released into the public domain.
*/

#include "Arduino.h"
#include "I2C.h"
#include <Wire.h>

void i2c_init(){
    Wire.begin(); //Join the bus as a master
    Serial.println("I2C bus initialized.");
};

// Write a single byte of data to a register
void writeRegister(byte dev_address, byte reg, byte data){
	writeRegisters(dev_address, reg, &data, 1);
}

// Write an array of "len" bytes ("buffer"), starting at register "reg", and
// auto-incrmenting to the next.
void writeRegisters(byte dev_address, byte reg, byte *buffer, byte len){
	Wire.beginTransmission(dev_address);
	Wire.write(reg);
	for (int x = 0; x < len; x++)
		Wire.write(buffer[x]);
	Wire.endTransmission(); //Stop transmitting
}

// Read a byte from the device register "reg".
byte readRegister(byte dev_address, byte reg){
	Wire.beginTransmission(dev_address);
	Wire.write(reg);
	Wire.endTransmission(false); //endTransmission but keep the connection active

	Wire.requestFrom(dev_address, (byte) 1); //Ask for 1 byte, once done, bus is released by default

	while(!Wire.available()) ; //Wait for the data to come back

	return Wire.read(); //Return this one byte
}
    
// Read "len" bytes from the device, starting at register "reg". Bytes are stored
// in "buffer" on exit.
void readRegisters(byte dev_address, byte reg, byte *buffer, byte len){
	Wire.beginTransmission(dev_address);
	Wire.write(reg);
	Wire.endTransmission(false); //endTransmission but keep the connection active

	Wire.requestFrom(dev_address, len); //Ask for bytes, once done, bus is released by default

	while(Wire.available() < len); //Hang out until we get the # of bytes we expect

	for(int x = 0 ; x < len ; x++)
		buffer[x] = Wire.read();    
}