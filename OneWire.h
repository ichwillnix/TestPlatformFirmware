#ifndef OneWire_h
#define OneWire_h

#include <Arduino.h>
#include "DS2483.h" //The I2C <-> OneWire bridge IC

class OneWire
{
public:	
	OneWire(); // Constructor
	void init(void);
  byte readDS18B20(float *temp1, float *temp2, float *temp3);
	void print(void);
private:

};

#endif
