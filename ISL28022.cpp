#include "ISL28022.h"
#include <Arduino.h>
#include "I2C.h"
#include "Error.h"
#include "types.h"

uint16_t register_val; //temp variable holding a complete 16bit register
byte overflow = false; //true when voltage or current overflows

//This chip sends/receives the LSB first.
uint16_t reverse_int(uint16_t to_reverse){
	byte A = to_reverse & 0xFF;
	byte B = (to_reverse>>8) & 0xFF;
	return (((uint16_t)A)<<8) + B;
}

// CONSTRUCTOR
ISL28022::ISL28022()
{
}

// Resistors (KRL2012E-M-R003-F-T5) are 1W, so at 0.003Ohm they can accept 18A each, so 72A max continuous for the board. But they are placed very close to strong heatsinks, so we allow higher burst current.
#define OVERRANGEVAL 1000 //1000 A will be read when the chip is staturated (safer then returning dummy values)
#define SHUNTRANGEMV 160 //40 80 160 320   (limit in current for our board before chip saturates: 53A, 106A, 213A, 426A
#define RESISTANCE003 (0.003/4.0) //0.003 shunt
#define RESISTANCE004 (0.004/4.0) //0.004 shunt
#define CURRFS003 ((0.001*SHUNTRANGEMV)/RESISTANCE003) //Eq. 2 datasheet
#define CURRFS004 ((0.001*SHUNTRANGEMV)/RESISTANCE004) //Eq. 2 datasheet
#define CURRLSB003 (CURRFS003/32768.0) //Eq. 3 datasheet
#define CURRLSB004 (CURRFS004/32768.0) //Eq. 3 datasheet
#define CALREGVAL003 ((uint16_t)((0.04096/(CURRLSB003*RESISTANCE003))+0.5)) //Eq. 4 datasheet, +0.5 is for rounding to integer
#define CALREGVAL004 ((uint16_t)((0.04096/(CURRLSB004*RESISTANCE004))+0.5)) //Eq. 4 datasheet, +0.5 is for rounding to integer

//Example:
  // 40mV
  // Eq. 2 datasheet: CurrentFS = 0.04/0.00075 = 53.333A // Max measurable current
  // Eq. 3 datasheet: CurrentLSB = 53.3333/32768 = 0.0016276 A/LSB
  // Eq. 4 datasheet: CalRegVal = integer(0.04096/(0.0016276*0.00075)) = 33554

// INITIALIZATION
// Returns a 0 if communication failed, 1 if successful.
byte ISL28022::init()
{
  /*Serial.print("SHUNTRANGEMV:");
  Serial.println(SHUNTRANGEMV,10);
  Serial.print("RESISTANCE:");
  Serial.println(RESISTANCE,10);
  Serial.print("CURRFS:");
  Serial.println(CURRFS,10);
  Serial.print("CURRLSB:");
  Serial.println(CURRLSB,10);
  Serial.print("CALREGVAL:");
  Serial.println(CALREGVAL,10);*/
  
  // Read CONFIG register, reversed order
	readRegisters(ISL28022_I2C_Address,CONFIG,(byte*) &register_val,2);  
	register_val = reverse_int(register_val);

  if( register_val==0x719F || register_val==0x799F || register_val==0x619F){
     //Serial.println("ISL28022 0K");
  }else{
	  Serial.print("ISL28022 PROBLEM: ");
	  Serial.print(" 0x");
	  Serial.println(register_val,HEX);
	  error(ISL28022_Init);
	  return 0;
  }

#if SHUNTRANGEMV==40
  //Set shunt voltage range to 80mV  
  bitClear(register_val,PG1);
  bitClear(register_val,PG0);
#elif SHUNTRANGEMV==80
  //Set shunt voltage range to 80mV  
  bitClear(register_val,PG1);
  bitSet(register_val,PG0);
#elif SHUNTRANGEMV==160
  //Set shunt voltage range to 160mV  
  bitSet(register_val,PG1);
  bitClear(register_val,PG0);
#elif SHUNTRANGEMV==320
  //Set shunt voltage range to 80mV  
  bitSet(register_val,PG1);
  bitClear(register_val,PG0);
#else
  # error "Wrong selection of PGA setting"
#endif

	//Write back config register
	register_val = reverse_int(register_val);
	writeRegisters(ISL28022_I2C_Address,CONFIG,(byte*) &register_val,2);

  //Set the shunt value in the chip
  if(board_info.version==3){
    //0.004 Ohm shunt
    register_val = reverse_int(CALREGVAL004);
  }else{
    //0.003 Ohm shunt
    register_val = reverse_int(CALREGVAL003);
  } 
	writeRegisters(ISL28022_I2C_Address, CALIB,(byte*)&register_val, 2);

	return 1;
}

//Returns bus voltage in V
float ISL28022::voltage(void){ 
	// Read BUS VOLTAGE register
	readRegisters(ISL28022_I2C_Address,BUS_V,(byte*) &register_val,2);  
	register_val = reverse_int(register_val);

	//Following formula on datasheet, with default settings
	uint16_t voltage = 0;
	uint16_t exponent = 1;
	byte i;
	for(i=2;i<=15;i++){
		voltage += bitRead(register_val,i) * exponent;
		exponent *= 2;
	}

  overflow = bitRead(register_val,0);

  if(overflow){
    error(ISL28022_Overflow);
    return 60.0;
  }else{
    return 0.004*voltage;
  }	
}

//Returns shunt voltage in 10uV steps, signed
int16_t ISL28022::shuntVoltage(void){
	// Read SHUNT VOLTAGE register
	readRegisters(ISL28022_I2C_Address,SHUNT_V,(byte*) &register_val,2);  
	register_val = reverse_int(register_val);

	//Following formula on datasheet, with 40mV range setting
	int16_t voltage = 0;
	/*uint16_t exponent = 1;
	byte i;
	for(i=0;i<=11;i++){
		voltage += bitRead(register_val,i) * exponent;
		exponent *= 2;
	}
	voltage -= bitRead(register_val,12) * exponent;*/

	return voltage;
} 

//Returns current
float ISL28022::current(void){
	// Read CURRENT register
	readRegisters(ISL28022_I2C_Address, CURRENT, (byte*)&register_val, 2);
	register_val = reverse_int(register_val);

	//Eq. 5 datasheet
	int32_t current = 0;
	uint16_t exponent = 1;
	byte i;
	for (i = 0; i <= 14; i++){
		current += bitRead(register_val, i) * exponent;
		exponent *= 2;
	}
	current -= bitRead(register_val, 15) * exponent;

  //Based on shunt value in the chip
  if(board_info.version==3){
    //0.004 Ohm shunt
    if(overflow){
      return CURRFS004; 
    }else{
      return CURRLSB004*current;
    }  
  }else{
    //0.003 Ohm shunt
    if(overflow){
      return CURRFS003; 
    }else{
      return CURRLSB003*current;
    }  
  }
}

//Returns power
float ISL28022::power(void){
  // Read CURRENT register
  readRegisters(ISL28022_I2C_Address, POWER, (byte*)&register_val, 2);
  register_val = reverse_int(register_val);

  //Eq. 6 datasheet
  uint16_t power = 0;
  uint16_t exponent = 1;
  byte i;
  for (i = 0; i <= 15; i++){
    power += bitRead(register_val, i) * exponent;
    exponent *= 2;
  }

  //Based on shunt value in the chip
  if(board_info.version==3){
    //0.004 Ohm shunt
    if(overflow){
      return 60.0*CURRFS004;  
    }else{
      return 2.0*5000.0*0.004*CURRLSB004*power;
    }  
  }else{
    //0.003 Ohm shunt
    if(overflow){
      return 60.0*CURRFS003;  
    }else{
      return 2.0*5000.0*0.004*CURRLSB003*power;
    }  
  }
}
