#include <Arduino.h>
#include "POWER_SENSORS.h"

POWER_SENSORS::POWER_SENSORS(){}

ISL28022 powerIC;

void POWER_SENSORS::init(){
	powerIC.init();
}

void POWER_SENSORS::print(){
	Serial.print("Power IC: ");
	float V = powerIC.voltage();
	Serial.print("Supply ");
	Serial.print(V, 3);
	Serial.print("V, \t ");
	V = powerIC.shuntVoltage()*0.01;
	Serial.print("Shunt ");
	Serial.print(V, 2);
	Serial.println("mV.");
}
