#include "Arduino.h"
#include "types.h"
#include "Protocol.h"
#include "RPM_SENSOR.h"
#include "AverageFilter.h"

/************************************** RCbenchmark Protocol *******************************************************/
// RCbenchmark Serial Protocol 0 
#define RCB_VERSION              0

//Protocol codes
#define RCB_DEBUG                   0 //debug1,debug2,debug3,debug4
#define RCB_FIRMWARE                1
#define RCB_BOARD    		    2
#define RCB_POLL		    3 //Polling message (ie continuously transmitted and received
#define RCB_GETOHM		    4 //Gets the latest ohmmeter reading.

#define INBUF_SIZE 64
uint8_t inBuf[INBUF_SIZE]; //Buffer receiving the command's data
uint8_t indRX; //Index reading the inBuf array
uint8_t checksum;
uint8_t cmdRCB;

//Sensor averaging filters (used to average all readings between USB communication cycles).
extern AverageFilter accxFilter;
extern AverageFilter accyFilter;
extern AverageFilter acczFilter;
extern AverageFilter accVibFilter;
extern AverageFilter leftLoadCellFilter;
extern AverageFilter rightLoadCellFilter;
extern AverageFilter thrustLoadCellFilter;
extern AverageFilter voltageFilter;
extern AverageFilter currentFilter;
extern AverageFilter powerFilter;
extern AverageFilter rpmFilter;
extern AverageFilter rpmOpticalFilter;
extern AverageFilter tempProbe1Filter;
extern AverageFilter tempProbe2Filter;
extern AverageFilter tempProbe3Filter;
extern AverageFilter pressurePFilter;
extern AverageFilter pressureTFilter;

void evaluateOtherData(uint8_t sr);
void evaluateCommand(uint8_t c);

void protocol_init(void){
	Serial.begin(115200);
}

static void serialize8(uint8_t a) {
	Serial.write(a);
	checksum ^= a;
}

static uint8_t read8()  {
	return inBuf[indRX++] & 0xff;
}

static void headSerialResponse(uint8_t err, uint8_t s) {
	serialize8('$');
	serialize8('R');
	serialize8(err ? '!' : '>');
	checksum = 0; // start calculating a new checksum
	serialize8(s);
	serialize8(cmdRCB);
}

static void headSerialReply(uint8_t s) {
	headSerialResponse(0, s);
}

static void headSerialError() {
	headSerialResponse(1, 0);
}

static void tailSerialReply() {
	serialize8(checksum);
}

static void serializeNames(PGM_P s) {
	headSerialReply(strlen_P(s));
	for (PGM_P c = s; pgm_read_byte(c); c++)
		serialize8(pgm_read_byte(c));
	tailSerialReply();
}

static void __attribute__((noinline)) s_struct_w(uint8_t *cb, uint8_t siz) {
	while (siz--) *cb++ = read8();
}

static void s_struct_partial(uint8_t *cb, uint8_t siz) {
	while (siz--) serialize8(*cb++);
}

static void s_struct(uint8_t *cb, uint8_t siz) {
	headSerialReply(siz);
	s_struct_partial(cb, siz);
	tailSerialReply();
}

static void mspAck() {
	headSerialReply(0); tailSerialReply();
}

enum RCB_protocol_bytes {
	COMM_IDLE,
	HEADER_START,
	HEADER_M,
	HEADER_ARROW,
	HEADER_SIZE,
	HEADER_CMD
};

void serialCom() {
	uint8_t c, cc, port, state, bytesTXBuff;
	static uint8_t offset;
	static uint8_t dataSize;
	static uint8_t c_state;

	cc = Serial.available();
	while (cc--) {
		c = Serial.read();
		state = c_state;
		// regular data handling to detect and handle RCB and other data
		if (state == COMM_IDLE) {
			if (c == '$') state = HEADER_START;
			else evaluateOtherData(c); // evaluate all other incoming serial data
		}
		else if (state == HEADER_START) {
			state = (c == 'R') ? HEADER_M : COMM_IDLE;
		}
		else if (state == HEADER_M) {
			state = (c == '<') ? HEADER_ARROW : COMM_IDLE;
		}
		else if (state == HEADER_ARROW) {
			if (c > INBUF_SIZE) {  // now we are expecting the payload size
				state = COMM_IDLE;
				continue;
			}
			dataSize = c;
			checksum = c;
			offset = 0;
			indRX = 0;
			state = HEADER_SIZE;  // the command is to follow
		}
		else if (state == HEADER_SIZE) {
			cmdRCB = c;
			checksum ^= c;
			state = HEADER_CMD;
		}
		else if (state == HEADER_CMD) {
			if (offset < dataSize) {
				checksum ^= c;
				inBuf[offset++] = c;
			}
			else {
				if (checksum == c) // compare calculated and transferred checksum
					evaluateCommand(cmdRCB); // we got a valid packet, evaluate it
				state = COMM_IDLE;
				cc = 0; // no more than one RCB per port and per cycle
			}
		}
		c_state = state;
	} // while
}

void evaluateCommand(uint8_t c) {
	uint32_t tmp = 0;
	byte payload_size;
        byte torque_update = false;
	switch (c) {
	case RCB_FIRMWARE:
		firmware_info.com_protocol_version = RCB_VERSION;
		s_struct((uint8_t*)&firmware_info, sizeof(firmware_info));
		break;
	case RCB_BOARD:
		s_struct((uint8_t*)&board_info, sizeof(board_info));
		break;
	case RCB_POLL:
		//Receive control data
		s_struct_w((uint8_t*)&basic_control, sizeof(basic_control));
		s_struct_w((uint8_t*)&pro_control, sizeof(pro_control));

		//Set the flags for the sensors that use the averaging filter
		basic_sensors.flag_esc_current = currentFilter.isData();
		basic_sensors.flag_esc_voltage = voltageFilter.isData();
    basic_sensors.flag_esc_power = powerFilter.isData();
		basic_sensors.flag_brushless_hz = rpmFilter.isData();
    basic_sensors.flag_optical_hz = rpmOpticalFilter.isData();
    basic_sensors.flag_temp_probe = tempProbe1Filter.isData(); //temp2 and temp3 update at the same time as temp1
    torque_update = leftLoadCellFilter.isData() && rightLoadCellFilter.isData();
		basic_sensors.flag_load_cell_left = torque_update;
		basic_sensors.flag_load_cell_thrust = thrustLoadCellFilter.isData();
		pro_sensors.flag_acc = accxFilter.isData(); //only need to do this for x, because y,z update at the same time for the acc.
		pro_sensors.flag_load_cell_right = torque_update;

		//Update sensors data that use the averaging filter 
		basic_sensors.esc_current = currentFilter.getAverage();
		basic_sensors.esc_voltage = voltageFilter.getAverage();
    basic_sensors.esc_power = powerFilter.getAverage();
		basic_sensors.brushless_hz = rpmFilter.getAverage();
    basic_sensors.optical_hz = rpmOpticalFilter.getAverage();
    basic_sensors.temp1 = tempProbe1Filter.getAverage();
    basic_sensors.temp2 = tempProbe2Filter.getAverage();
    basic_sensors.temp3 = tempProbe3Filter.getAverage();
		if(torque_update) basic_sensors.load_cell_left = leftLoadCellFilter.getAverage();
		basic_sensors.load_cell_thrust = thrustLoadCellFilter.getAverage();
		pro_sensors.accx = accxFilter.getAverage();
		pro_sensors.accy = accyFilter.getAverage();
		pro_sensors.accz = acczFilter.getAverage();
    pro_sensors.accVib = accVibFilter.getAverage();
        pro_sensors.pressure_P = pressurePFilter.getAverage();
        pro_sensors.pressure_T = pressureTFilter.getAverage();
		if(torque_update) pro_sensors.load_cell_right = rightLoadCellFilter.getAverage();

		//Send sensors data
		payload_size = sizeof(basic_sensors) + sizeof(pro_sensors);
		headSerialReply(payload_size);
		s_struct_partial((uint8_t*)&basic_sensors, sizeof(basic_sensors));
		s_struct_partial((uint8_t*)&pro_sensors, sizeof(pro_sensors));
		tailSerialReply();		

		last_poll_report_timestamp = millis();
		break;
	case RCB_GETOHM:
		if (ohmmeter.status == OHM_IDLE){
			ohmmeter.status = OHM_READING;
		}
		s_struct((uint8_t*)&ohmmeter, sizeof(ohmmeter));
		if (ohmmeter.status >= OHM_DONE_OK){
			ohmmeter.status = OHM_IDLE;
		}
		break;
	case RCB_DEBUG:
    payload_size = sizeof(debug) + sizeof(pin_states);
    headSerialReply(payload_size);
		s_struct_partial((uint8_t*)&debug, sizeof(debug));
    s_struct_partial((uint8_t*)&pin_states, sizeof(pin_states));
    tailSerialReply();
		break;
	default:  // we do not know how to handle the (valid) message, indicate error RCB $M!
		//headSerialError(); tailSerialReply();
		break;
	}
}

// evaluate all other incoming serial data
void evaluateOtherData(uint8_t sr) {
}
