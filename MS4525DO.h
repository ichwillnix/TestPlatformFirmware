#ifndef MS4525DO_h
#define MS4525DO_h

#include <Arduino.h>

#define MS4525DO_h_I2C_Address 0x28

////////////////////////////////
// MS4525DO Class Declaration //
////////////////////////////////
class MS4525DO
{
public:	
    MS4525DO(); // Constructor
	byte init();
    byte read();
    byte available();
    int16_t pressure_P;
    int16_t pressure_T;
    
private:

};

#endif